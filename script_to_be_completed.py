# -*- coding: utf-8 -*-
"""
Created on Fri Mar 10 09:16:36 2023

@author: Etienne Cheynet
"""
import numpy as np
import matplotlib.pyplot as plt
from functions import *
import scipy.io
from scipy import signal
import scipy.stats as stats
from scipy.signal import welch
from scipy.optimize import curve_fit
import seaborn as sns

plt.style.use('./custom_latex_style.mplstyle')
sns.set_style('whitegrid')

import os
if not os.path.exists('figures'):
   os.makedirs('figures')
   print(' The folder ''figures'' is created')
else:
    print(' The folder ''figures'' already exists')
   
# %% Load the data from the five masts
data= scipy.io.loadmat('Data_for_excercise_v2.mat')

u = np.squeeze(np.array(data['u']))
v = np.squeeze(np.array(data['v']))
w = np.squeeze(np.array(data['w']))
time = np.squeeze(np.array(data['t']))


# Remove NaNs if necessary
indNaN = np.argwhere(~np.isnan(np.mean(u,1)))
indNaN = indNaN[:,0]
time = time[indNaN]
v = v[indNaN,:]
w = w[indNaN,:]
u = u[indNaN,:]

# Get the number of sensors and time step
Nsensors = u[0,:].size
N = u[:,0].size

# %% Question 2
sampling_freq = 1 / (time[1] - time[0])
nyquist_freq = 1/2 * sampling_freq
print(N)
fundamental_freq = 1 / ((N-1) * (time[1] - time[0]))

print(f'Sampling frequency is {sampling_freq} hz')
print(f'Nyquist frequency is {nyquist_freq} hz')
print(f'Fundamental frequency is {fundamental_freq} hz')
# %% Question 3

fig, ax = plt.subplots(3, 1, figsize=(12,10), sharex=True)

ax[0].plot(time, u[:, 0], color='black')
ax[0].set_ylabel('u (m/s)')
ax[0].set_ylim(8, 22)

ax[1].plot(time, v[:, 0], color='red')
ax[1].set_ylabel('v (m/s)')
ax[1].set_ylim(-5, 5)

ax[2].plot(time, w[:, 0], color='blue')
ax[2].set_ylabel('w (m/s)')
ax[2].set_xlabel('time (s)')
ax[2].set_ylim(-4, 4)

plt.savefig('timeseries.pdf')

#Stationary test

vel_components = [u, v, w]        #Velocity components for mast 1
vel_names = ['u', 'v', 'w']
num_masts = 5


for n in range(num_masts):
    print(f'Results for mast {n+1}')
    v_detrended = remove_linear_trend(v[:, n], preserve_mean=False)
    u_detrended = remove_linear_trend(u[:, n], preserve_mean=False)
    w_detrended = remove_linear_trend(w[:, n], preserve_mean=False)
    mean = np.mean(np.sqrt(u[:, n]**2 + v[:, n]**2 + w[:, n]**2), axis=0)
    mean_detrended = np.mean(np.sqrt(u_detrended**2 + v_detrended**2 + w_detrended**2), axis=0)
    Nwin = int(300*sampling_freq)
    thres1 = 0.20
    thres2 = 0.40
    results_original = stationaryTest(u[:, n], time, Nwin, thres1, thres2, mean)
    results_detrended = stationaryTest(u_detrended, time, Nwin, thres1, thres2, mean_detrended)
    print(results_detrended)
    print(results_original)
    print('\n')
u_detrended = remove_linear_trend(u[:, 0], preserve_mean=True)
v_detrended = remove_linear_trend(v[:, 0], preserve_mean=True)
w_detrended = remove_linear_trend(w[:, 0], preserve_mean=True)

fig, ax = plt.subplots(3, 1, figsize=(12,10), sharex=True)
plt.suptitle('Time Series before and after Removing Linear Trend, mast 1')

ax[0].plot(time, u[:, 0], label='Original with Linear Trend', color='black')
ax[0].plot(time, u_detrended, label='Detrended Time Series', linestyle='--')
ax[0].set_ylabel("u' (m/s)")
ax[0].set_ylim(5, 25)
ax[0].legend()

ax[1].plot(time, v[:, 0], label='Original with Linear Trend', color='red')
ax[1].plot(time, v_detrended, label='Detrended Time Series', linestyle='--')
ax[1].set_ylabel("v' (m/s)")
ax[1].set_ylim(-10, 10)
ax[1].legend()

ax[2].plot(time, w[:, 0], label='Original with Linear Trend', color='green')
ax[2].plot(time, w_detrended, label='Detrended Time Series', linestyle='--')
ax[2].set_ylabel("w' (m/s)")
ax[2].set_xlabel('time (s)')
ax[2].set_ylim(-5, 5)
ax[2].legend()

plt.savefig('detrended.pdf')

# %% Question 4
u_skewness_list = []
v_skewness_list = []
w_skewness_list = []
u_kurtosis_list = []
v_kurtosis_list = []
w_kurtosis_list = []

for i in range(num_masts):
    u_detrended = remove_linear_trend(u[:, i], preserve_mean=True)
    v_detrended = remove_linear_trend(v[:, i], preserve_mean=True)
    w_detrended = remove_linear_trend(w[:, i], preserve_mean=True)

    u_skew = stats.skew(u_detrended)
    v_skew = stats.skew(v_detrended)
    w_skew = stats.skew(w_detrended)

    u_kurt = stats.kurtosis(u_detrended)
    v_kurt = stats.kurtosis(v_detrended)
    w_kurt = stats.kurtosis(w_detrended)

    u_skewness_list.append(u_skew)
    v_skewness_list.append(v_skew)
    w_skewness_list.append(w_skew)
    u_kurtosis_list.append(u_kurt)
    v_kurtosis_list.append(v_kurt)
    w_kurtosis_list.append(w_kurt)

u_skew_avg = np.mean(u_skewness_list)
v_skew_avg = np.mean(v_skewness_list)
w_skew_avg = np.mean(w_skewness_list)

u_kurt_avg = np.mean(u_kurtosis_list)
v_kurt_avg = np.mean(v_kurtosis_list)
w_kurt_avg = np.mean(w_kurtosis_list)

def check_gaussian(skewness, kurtosis):
    if abs(skewness) < 0.5 and abs(kurtosis) < 0.5:
        return "Yes"
    else:
        return "No"

u_gaussian = check_gaussian(u_skew_avg, u_kurt_avg)
v_gaussian = check_gaussian(v_skew_avg, v_kurt_avg)
w_gaussian = check_gaussian(w_skew_avg, w_kurt_avg)

print("Average skewness of u:", u_skew_avg)
print("Average skewness of v:", v_skew_avg)
print("Average skewness of w:", w_skew_avg)

print("Average kurtosis of u:", u_kurt_avg)
print("Average kurtosis of v:", v_kurt_avg)
print("Average kurtosis of w:", w_kurt_avg)

print("Is u Gaussian?", u_gaussian)
print("Is v Gaussian?", v_gaussian)
print("Is w Gaussian?", w_gaussian)
    

# %% Question 5
def turbulence_characteristics(u, v, w):
    mean = np.mean(np.sqrt(u**2 + v**2 + w**2), axis=0)
    sigma_u = np.std(u, axis=0)
    sigma_v = np.std(v, axis=0)
    sigma_w = np.std(w, axis=0)

    turb_int_u = (sigma_u / mean) * 100
    turb_int_v = (sigma_v / mean) * 100
    turb_int_w = (sigma_w / mean) * 100
    kurt_u = stats.kurtosis(u)
    kurt_v = stats.kurtosis(v)
    kurt_w = stats.kurtosis(w)

    skew_u = stats.skew(u)
    skew_v = stats.skew(v)
    skew_w = stats.skew(w)

    return mean, turb_int_u, turb_int_v, turb_int_w, kurt_u, kurt_v, kurt_w, skew_u, skew_v, skew_w

for i in range(num_masts):
    u_detrended = remove_linear_trend(u[:, i], preserve_mean=True)
    v_detrended = remove_linear_trend(v[:, i], preserve_mean=True)
    w_detrended = remove_linear_trend(w[:, i], preserve_mean=True)  
    mean, turb_int_u, turb_int_v, turb_int_w, kurt_u, kurt_v, kurt_w, skew_u, skew_v, skew_w = turbulence_characteristics(u_detrended, v_detrended, w_detrended)
    print(f'The mean wind speed of mast {i+1} is {mean}')
    print(f'Turbulence intensity of mast {i+1} is {turb_int_u} for u, {turb_int_v} for v, {turb_int_w} for w')
    print(f'Kurtosis of u-wind component of mast {i+1} is {3+kurt_u}')
    print(f'Kurtosis of v-wind component of mast {i+1} is {3+kurt_v}')
    print(f'Kurtosis of w-wind component of mast {i+1} is {3+kurt_w}')
    print(f'Skewness of u-wind component of mast {i+1} is {skew_u}')
    print(f'Skewness of v-wind component of mast {i+1} is {skew_v}')
    print(f'Skewness of w-wind component of mast {i+1} is {skew_w}')

# %% Question 6

u = np.squeeze(np.array(data['u']))
v = np.squeeze(np.array(data['v']))
w = np.squeeze(np.array(data['w']))
time = np.squeeze(np.array(data['t']))
indNaN = np.argwhere(~np.isnan(np.mean(u,1)))
indNaN = indNaN[:,0]
time = time[indNaN]
v = v[indNaN,:]
w = w[indNaN,:]
u = u[indNaN,:]

fric_vel, R = frictionVelocity(u[:, 0], v[:, 0], w[:, 0])

print(f'Friction velocity of mast 1 is {fric_vel}')
print(f'Reynolds stress tensor of mast 1 is {R}')

# %% Question 7

from scipy.signal import welch, csd

nperseg = len(time) // 16 
noverlap = nperseg // 2

u = np.squeeze(np.array(data['u']))
v = np.squeeze(np.array(data['v']))
w = np.squeeze(np.array(data['w']))

Components = [u, v, w]        
names = ['u', 'v', 'w']

spectra_u = []
spectra_v = []
spectra_w = []
spectra_re = []
for component_index in range(3):
    spectra = []
    for mast_index in range(num_masts):
        frequency, spectrum = welch(Components[component_index][:, mast_index], fs=5, nperseg=nperseg, noverlap=noverlap)
        spectra.append(spectrum)
    if component_index == 0:
        spectra_u = spectra
    elif component_index == 1:
        spectra_v = spectra
    elif component_index == 2:
        spectra_w = spectra
    freq_re, spec_re = csd(remove_linear_trend(Components[0][:, mast_index], preserve_mean=True), remove_linear_trend(Components[2][:, mast_index], preserve_mean=True), fs=5, nperseg=len(time) // 2)
    spectra_re.append(spec_re)

mean_spectrum_u = np.mean(spectra_u, axis=0) / fric_vel**2
mean_spectrum_v = np.mean(spectra_v, axis=0) / fric_vel**2
mean_spectrum_w = np.mean(spectra_w, axis=0) / fric_vel**2
mean_spectrum_re = np.mean(spectra_re, axis=0) / fric_vel**2

plt.figure(figsize=(10, 5))
plt.semilogx(freq_re, mean_spectrum_re, label='i=w', linestyle='-')
plt.semilogx(frequency, mean_spectrum_u, label='i=u', linestyle='-')
plt.semilogx(frequency, mean_spectrum_v, label='i=v', linestyle='-')
plt.semilogx(frequency, mean_spectrum_w, label='i=Re', linestyle='-')

plt.xlim(1e-2, 1e1)
plt.ylim(-0.5, 1.0)
plt.title('Mean PSD for all masts')
plt.xlabel('Frequency (Hz)')
plt.ylabel('$PSD (fS_{i}/u_*^2)$')
plt.legend()
plt.savefig('semilogplot.pdf')

u = np.squeeze(np.array(data['u']))
v = np.squeeze(np.array(data['v']))
w = np.squeeze(np.array(data['w']))
time = np.squeeze(np.array(data['t']))
indNaN = np.argwhere(~np.isnan(np.mean(u,1)))
indNaN = indNaN[:,0]
time = time[indNaN]
v = v[indNaN,:]
w = w[indNaN,:]
u = u[indNaN,:]
u_detrended = remove_linear_trend(u[:, 0], preserve_mean=False)
v_detrended = remove_linear_trend(v[:, 0], preserve_mean=False)
w_detrended = remove_linear_trend(w[:, 0], preserve_mean=False)
mean = np.mean(np.sqrt(u_detrended**2 + v_detrended**2 + w_detrended**2))

plt.figure(figsize=(10, 8))
frequency, spec_u = welch(u_detrended, fs=sampling_freq, nperseg=nperseg, noverlap=noverlap)
_, spec_v = welch(v_detrended, fs=sampling_freq, nperseg=nperseg, noverlap=noverlap)
_, spec_w = welch(w_detrended, fs=sampling_freq, nperseg=nperseg, noverlap=noverlap)
_, spec_re = csd(u_detrended, w_detrended, fs=sampling_freq, nperseg=nperseg, noverlap=noverlap)

plt.semilogx(frequency/mean, spec_u/fric_vel**2, label='i=u', color='red')
plt.semilogx(frequency/mean, spec_v/fric_vel**2, label='i=v', color='blue')
plt.semilogx(frequency/mean, spec_w/fric_vel**2, label='i=w', color='green')
plt.semilogx(frequency/mean, spec_re/fric_vel**2, label='i=Re', color='black')

plt.xlabel(r'$ \frac{f}{\bar{U}} $')    
plt.ylim(-0.2, 1)
plt.xlim(1e-2, 1)
plt.ylabel('$PSD (fS_{i}/u_*^2)$')
plt.title('PSD for mast 1')
plt.legend()

plt.savefig('semilog_mast1.pdf')

# Question 8

nperseg = len(time) // 10 
noverlap = nperseg // 2

fig, ax = plt.subplots(figsize=(10, 8)) 

freq, S_u = welch(u_detrended, fs=5, scaling='density', nperseg=nperseg, noverlap=noverlap)
_, S_v = welch(v_detrended, fs=5, scaling='density', nperseg=nperseg, noverlap=noverlap)
_, S_w = welch(w_detrended, fs=5, scaling='density', nperseg=nperseg, noverlap=noverlap)

Sw_Su = S_w / S_u
Sv_Su = S_v / S_u

ax.semilogx(freq/mean, Sw_Su, label='$S_w/S_u$')
ax.semilogx(freq/mean, Sv_Su, label='$S_v/S_u$')

ax.axhline(y=4/3, color='r', linestyle='-', label='4/3')

ax.set(xlim=(1e-3, 0), ylim=(0, 3))

ax.set_xlabel(r'$ \frac{f}{\bar{U}} $')
ax.set_ylabel('$PSD$')
ax.legend()

plt.savefig('spectra_ratios.pdf')

# Question 9
fs = 5  
Cu = np.arange(3, 21, 1)
dy = 12.5
mean_u = 15

plt.figure(figsize=(10, 8))
for idx, i in enumerate(Cu):
    gamma_u = np.exp(-int(i) * dy * freq / mean_u)
    plt.semilogx(freq, gamma_u, label=f'Cu={i}')

plt.xlabel('Frequency (Hz)')
plt.ylabel('Turbulence coherence')
plt.legend()
plt.savefig('davenport.pdf')

# Question 10

u = np.squeeze(np.array(data['u']))
v = np.squeeze(np.array(data['v']))
w = np.squeeze(np.array(data['w']))

mean_u = np.mean(u)
u_1 = remove_linear_trend(u[:, 0], preserve_mean=True)
u_2 = remove_linear_trend(u[:, 1], preserve_mean=True)

segments = [4, 6, 8, 10, 30, 60]    

fs = 5  
Cu = 7
dy = 12.5

fig, ax = plt.subplots(len(segments), 1, figsize=(10,15), sharex=True)

for i, segment in enumerate(segments):
    axs = ax[i]
    N = len(u_1) // segment
    co_coherence, quad_coherence, freq = coherence(u_1, u_2, N, fs)
    gamma_u = np.exp(-Cu * dy * freq / mean_u)
    axs.semilogx(freq, co_coherence, label=f'{segment} segments')
    axs.semilogx(freq, gamma_u, label='Theoretical Coherence', linestyle='--', color='red')
    if segment==60:
        axs.set(xlim=(1e-3, 1e1))
        axs.set_xlabel(r'$ \frac{f}{\bar{U}} $')
    axs.legend()

fig.text(0.04, 0.5, 'Coherence', va='center', rotation='vertical')
plt.savefig('co_coherence.pdf')


    



